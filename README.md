# IIT KGP FOUNDATION OF AIML COURSE VIDEOS

## VIDEO SET 4

| SL# | TOPICS |
|:---:| :----- |
|  1  | Deep Learning Part 1 |
|  2  | Deep Learning Part 2 |
|  3  | Intro to Library Part 1 |
|  4  | Intro to Library Part 2 |
|  5  | Machine Learning - 2 |
|  6  | Machine Learning - 3 part 1 |
|  7  | Machine Learning - 3 part 2 |